class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :title
      t.integer :price
      t.integer :currency_id
      t.integer :genre_id
      t.integer :language_id
      t.string :author
      t.integer :year
      t.integer :pages

      t.timestamps
    end
  end
end
