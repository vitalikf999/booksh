class CreateLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :languages do |t|
      t.string :title
      t.integer :book_id

      t.timestamps
    end
  end
end
