class CreateCurrencies < ActiveRecord::Migration[5.1]
  def change
    create_table :currencies do |t|
      t.string :title
      t.integer :book_id

      t.timestamps
    end
  end
end
