# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#languages
Language.create!(title: 'English')
Language.create!(title: 'Ukrainian')
Language.create!(title: 'Russian')
#genres
Genre.create!(title: 'Action and adventure')
Genre.create!(title: 'Children s literature')
Genre.create!(title: 'Biography')
Genre.create!(title: 'Cookbook')
Genre.create!(title: 'Crime')
Genre.create!(title: 'Drama')
Genre.create!(title: 'Encyclopedia')
Genre.create!(title: 'Guide')
Genre.create!(title: 'Health')
Genre.create!(title: 'Fantasy')
Genre.create!(title: 'Horror')
Genre.create!(title: 'Mystery')
Genre.create!(title: 'Poetry')
Genre.create!(title: 'Political thriller')
Genre.create!(title: 'Thriller')
Genre.create!(title: 'Travel')
#currencies
Currency.create!(title: '$')
Currency.create!(title: '₴')
Currency.create!(title: '₽')
