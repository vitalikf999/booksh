class Book < ApplicationRecord
  belongs_to :user
  has_one :currency
  has_one :language
  has_one :genre
  has_many :comments

  mount_uploaders :photos, PhotoUploader
  serialize :photos, JSON # If you use SQLite, add this line.

  validates :title, :price, :description, :year, :pages, :author, presence: true
  validates :price, :year, :pages, numericality: { only_integer: true, message: "only allows numericality" }
end
