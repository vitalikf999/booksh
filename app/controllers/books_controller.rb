class BooksController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :find_book, only: [:show, :edit, :update, :destroy]
  def index
    @books = Book.all
  end

  def new
    @book = Book.new
  end

  def create
    @book = current_user.books.build(book_param)
    if @book.save
      flash[:notice] = "Successfully created"
      redirect_to book_path(@book.id)
    else
      flash[:error] = "Has error with created"
      render 'new'
    end
  end

  def show
    @comments = @book.comments
  end

  def edit
  end

  def update
    if @book.update(book_param)
      flash[:notice] = "Successfully updated"
      redirect_to book_path(@book.id)
    else
      flash[:error] = "Has error with created"
      render 'edit'
    end
  end

  def destroy
    @book.destroy
    redirect_to root_path
  end
private
  def book_param
    params.require(:book).permit(:user_id, :title, :description, :price, :currency_id, :genre_id, :language_id, :author, :year, :pages, {photos: []})
  end

  def find_book
   @book = Book.find_by(id: params[:id])
 end
end
