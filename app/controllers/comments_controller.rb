class CommentsController < ApplicationController
  before_action :authenticate_user!
before_action :set_book

  def new
    @comment = Comment.new
  end

  def create
    params[:comment][:user_id] = current_user.id
    params[:comment][:book_id] = @book.id
    @comment = Comment.new(comment_params)
    if @comment.save
      redirect_to book_path(@book.id)
    else
      render 'new'
    end

  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end

private
  def set_book
    @book = Book.find_by(id: params[:book_id])
  end

  def comment_params
    params.require(:comment).permit(:user_id, :book_id, :body)
  end
end
