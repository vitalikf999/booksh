Rails.application.routes.draw do
  get 'comments/new'

  devise_for :users
  resources :books do
      resources :comments
  end
  root 'books#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
